from django.urls import path
from . import views

urlpatterns = [
    path('users/', views.UserList.as_view(), name='user'),
    path('metrics/', views.metrics, name='metrics')
   ]