﻿Features:

	Python 3.5
	Latest Django 2.0.6
	Support for Dajngo Rest Framework
	SQLite for Database
	Prometheus for monitoring the api’s

Project Setup:

	setup prometheus ->
	Download prometheus tool  (Monitoring tool)  ->   https://prometheus.io/download/
    Extract the folder and repalce the prometheus.yml file by file present in git folder.
	create and activate virtual environment
	pip install -r requirements.txt
	python manage.py makemigrations
	python manage.py migrate
	python manage.py runserver

Configured URLs:

	localhost:8000/admin
	Username: root
	password: admin123
	Description: URL for django admin panel. Enter localhost:8000/admin on 	browser and click on demoapp and users.In the search box you can 	filter the users by email,city,firstname and date.

	localhost:8000/users
	Description: URL to search and create new users. 
	Usage: localhost:8000/users will give all users
	       locahost:8000/users/?userid=1 will filter user according to userid.
		   locahost:8000/users/?date=2018-06-20 will filter users according to date.
	       localhost::8000/users/?city=pune will filter users according to city.

	To create new user :

	Hit localhost:8000/users/ from tool postman or restclient
	Select post method and in body enter the data.
	Sample json data will be :
	
	{
		"first_name": "sampleuser",
		"last_name": "test",
		"email": "c7@gmail.com",
		"city": "banglore"
	}
	
	To monitor apis hit the below url for prometheus:
		localhost:9090/
			click on graph tab from navbar
			in input field below enter the below strings to see the api count on graph
			  - "my_requests_total"

Note: Refer to screenshot folder for screenshots of api's output



	





